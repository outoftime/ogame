﻿AvailableTransport = {
	'ЛИ': 'am204', 
	'ТИ': 'am205', 
	'Крейсер': 'am206', 
	'Линкор': 'am207',
	'Линейка': 'am215', 
	'Бомбардировщик': 'am211', 
	'Уник': 'am213', 
	'Звезда': 'am214', 
	'МТ': 'am202',
	'БТ': 'am203', 
	'Колонизатор': 'am208', 
	'Переработчик': 'am209', 
	'ШЗ': 'am210',
}

AvailableMissions = {
	'Экспедиция': 15,
	'Колонизировать': 7,
	'Переработать': 8,
	'Транспорт': 3,
	'Оставить': 4,
	'Шпионаж': 6,
	'Держаться': 5,
	'Атака': 1,
	'Совместная атака': 2,
	'Уничтожить': 9,
}

Planets = {
	'Oblivion': '33625607',
	'Zenon': '33630797',
	'Terranova': '33649633',
	'Chronos': '33652427',
	'Nemesis': '33669019',
}

Auth = {}

Run = [
	'Oblivion_expedition',
	'Terranova_expedition',
]

Missions = {
	'1_178_10_spy': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'ШЗ',
				'Count': 1,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 378,
			'Position': 10,
		},
		'Type': 'Шпионаж',
	},
	'1_381_10_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 381,
			'Position': 10,
		},
		'Type': 'Атака',
		#'Retry': 3*60*60,
	},	
	'1_381_4_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 381,
			'Position': 4,
		},
		'Type': 'Атака',
	},	
	'1_381_11_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 381,
			'Position': 11,
		},
		'Type': 'Атака',
	},	
	'1_383_3_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 383,
			'Position': 3,
		},
		'Type': 'Атака',
		#'Retry': 3*60*60,
	},	
	'1_380_10_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 380,
			'Position': 10,
		},
		'Type': 'Атака',
	},	
	'1_380_8_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 380,
			'Position': 8,
		},
		'Type': 'Атака',
	},		
	'1_378_6_attack': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'Линкор',
				'Count': 5,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 378,
			'Position': 6,
		},
		'Type': 'Атака',
	},	
	'Oblivion_expedition': {
		'Planet': 'Oblivion',
		'Transport': [
			{
				'Name': 'ЛИ',
				'Count': 30,
			},
			{
				'Name': 'ТИ',
				'Count': 18,
			},
			{
				'Name': 'БТ',
				'Count': 30,
			},
			{
				'Name': 'ШЗ',
				'Count': 3,
			},
		],
		'Location': {
			'Galaxy': 1,
			'System': 378,
			'Position': 16,
		},
		'Expedition_time': 1,
		'Type': 'Экспедиция',
		'Retry': 3*60*60,
	},
	'Terranova_expedition': {
		'Planet': 'Terranova',
		'Transport': [
			{
				'Name': 'ЛИ',
				'Count': 90,
			},
			{
				'Name': 'БТ',
				'Count': 40,
			},
			{
				'Name': 'ШЗ',
				'Count': 3,
			},
		],
		'Location': {
			'Galaxy':	1,
			'System':	377,
			'Position':	16,
		},
		'Expedition_time': 1,
		'Type': 'Экспедиция',
		'Retry': 3*60*60,
	},
}

import os.path

if os.path.exists('login.config'):
	import bz2
	import pickle
	
	Auth = pickle.loads(bz2.BZ2File('login.config').read())
	
else:
	print 'File "login.config" doesn\'t exist. Create it using "create_login.bat".'
	raw_input('Press <Enter> to exit.')
	
	import sys
	sys.exit(0)

if False:
	import easygui as eg

	msg     = "Select missions."
	title   = "Missions"
	choices = Missions.keys()
	Run		= eg.multchoicebox(msg, title, choices) or []
	
	if len(Run) == 0:
		import sys
		sys.exit(0)

