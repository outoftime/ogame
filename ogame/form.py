from bs4 import BeautifulSoup

class form:
	def __init__(self, html, form_name):
		soup = BeautifulSoup(html)
		self.form = soup.find('form', attrs={'name': form_name})

	def get_data(self):
		inputs = self.form('input', attrs={'disabled': False})
		data = {}
		for input in inputs:
			data[input['name']] = input['value']
		return data
