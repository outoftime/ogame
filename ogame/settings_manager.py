from settings import AvailableTransport, AvailableMissions, Planets, Auth, Run, Missions

class manager:
	def get_user(self):
		return Auth['user']
		
	def get_pass(self):
		return Auth['pass']

	def get_missions(self):
		for name in Run:
			if (Missions.has_key(name)):
				yield Missions[name]
				
	def get_planet(self, name):
		return Planets[name]
	
	def get_data(self, mission):
		d = {
			'galaxy': mission['Location']['Galaxy'],
			'system': mission['Location']['System'],
			'position': mission['Location']['Position'],
			'mission': AvailableMissions[mission['Type']],
			'speed': 10,
		}
		for transport in mission['Transport']:
			d[AvailableTransport[transport['Name']]] = transport['Count']
			
		if mission.has_key('Resourses'):
			d['metal'] = mission['Resourses']['Metal']
			d['crystal'] = mission['Resourses']['Crystal']
			d['deuterium'] = mission['Resourses']['Deuterium']
		
		if mission.has_key('Expedition_time'):
			d['expeditiontime'] = mission['Expedition_time']
			
		if mission.has_key('Speed'):
			d['speed'] = mission['Speed']
			
		return d