import time
import random
import threading

class timer(threading.Thread):
	def __init__(self, interval, callback, args):
		self.interval = interval
		self.callback = callback
		self.args = args
		threading.Thread.__init__(self)
	
	def run(self):
		time.sleep(self.interval) #+ random.randint(0, 10*60) - 5*60)
		self.callback(self.args)