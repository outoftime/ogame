import requests
import ogame.form
import ogame.settings_manager
from threading import Timer
from time import sleep

def extend(destination, source):
	for key in destination.keys():
		if (source.has_key(key)):
			destination[key] = source[key]
	return destination

def retry(bot, mission):
	bot.session = bot.get_session()
	bot.send(mission)
	
class bot:
	def __init__(self):
		self.settings = ogame.settings_manager.manager()
		
	def begin(self):
		self.session = self.get_session()
		for mission in self.settings.get_missions():
			self.send(mission)
		print 'Finished.'

	def get_session(self):
		headers = {
			'Host': 'uni109.ogame.ru',
			'Connection': 'keep-alive',
			'Origin': 'http://uni109.ogame.ru',
			'User-Agent': 'Mozilla/5.0 (Windows NT 5.1; rv:11.0) Gecko/20100101 Firefox/11.0',
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		}
		session = requests.session(headers = headers)
		session.get('http://uni109.ogame.ru/game/reg/login2.php?v=2&login={0}&pass={1}'.format(self.settings.get_user(), self.settings.get_pass()))
		return session
		
	def get_fleet_url(self, planet_name):
		return 'http://uni109.ogame.ru/game/index.php?page=fleet1&cp={0}'.format(self.settings.get_planet(planet_name))
		
	def send_internal(self, data, html, form_name, url):
		f = ogame.form.form(html, form_name)
		data = extend(f.get_data(), data)
		return self.session.post(url, data)
		
	def send(self, mission):
		print 'Executing mission: from "{0}" {1}:{2}:{3}'.format(
			mission['Planet'], 
			mission['Location']['Galaxy'], 
			mission['Location']['System'], 
			mission['Location']['Position'],
		)
		d = self.settings.get_data(mission)
		
		url = self.get_fleet_url(mission['Planet'])
		r = self.session.get(url) 
		
		steps = [
			{ 
				'form_name': 'shipsChosen', 
				'url': 'http://uni109.ogame.ru/game/index.php?page=fleet2', 
			},
			{ 
				'form_name': 'details', 
				'url': 'http://uni109.ogame.ru/game/index.php?page=fleet3', 
			},
			{ 
				'form_name': 'sendForm', 
				'url': 'http://uni109.ogame.ru/game/index.php?page=movement', 
			},
		]
		
		html = r.content
		for step in steps:
			html = self.send_internal(d, html, step['form_name'], step['url']).content
			
		if 'Retry' in mission:
			timer = Timer(mission['Retry'], retry, args=[self, mission])
			timer.start()
			sleep(5)
