from time import sleep
from subprocess import call
from argparse import ArgumentParser

parser = ArgumentParser(description='Start "run.py" after entered time interval.')
parser.add_argument('-d', '--delay', metavar='delay', type=int, nargs='?', help='Number of seconds before started subprocess.')
args = vars(parser.parse_args())

try:
    delay = args['delay'] or int(raw_input('Type number of second before started subprocess: '))
except ValueError:
    print 'Entered value is not an integer.'
else:
    sleep(delay)
    path = 'python -OO run.py'
    call(path, shell=True)

