from bz2 import compress
from pickle import dumps
from argparse import ArgumentParser

parser = ArgumentParser(description='Creates encrypted "login.config" with personal info.')
parser.add_argument('-u', '--user', metavar='user', type=unicode, nargs='?', help='Username.')
parser.add_argument('-p', '--pass', metavar='pass', type=unicode, nargs='?', help='User\'s password.')
args = vars(parser.parse_args())

Auth = {
    'user': args['user'] or raw_input('Type your user name: '),
    'pass': args['pass'] or raw_input('Type your user password: '),
}

with open('login.config', 'w') as file:
    file.write(compress(dumps(args)))

